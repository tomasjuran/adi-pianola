#define NOTE_C 16.35
#define NOTE_D 18.35
#define NOTE_E 20.6
#define NOTE_F 21.83
#define NOTE_G 24.5
#define NOTE_A 27.5
#define NOTE_B 30.87

#define SHARP_C 17.32
#define SHARP_D 19.45
#define SHARP_E 21.83
#define SHARP_F 23.13
#define SHARP_G 25.96
#define SHARP_A 29.14
#define SHARP_B 32.7

// Notas por índice en la lista: 0 = silencio, 1 = Do, 2 = Re, ... , 7 = Si
float NOTES[8] = {0, NOTE_C, NOTE_D, NOTE_E, NOTE_F, NOTE_G, NOTE_A, NOTE_B};
float SHARP[8] = {0, SHARP_C, SHARP_D, SHARP_E, SHARP_F, SHARP_G, SHARP_A, SHARP_B};

/* Pin del buzzer */
#define BUZZER 5

/* Pines de los lectores IR, de izquierda a derecha */
#define IRL 6
#define IRM 7
#define IRR 8

int READERS[3] = {IRR, IRM, IRL};
int READERS_LEN = sizeof(READERS) / sizeof(READERS[0]);

/* Pines del motor paso a paso */
#define IN1 9
#define IN2 10
#define IN3 11
#define IN4 12

/* Delay por defecto entre pasos */
#define STEP_DELAY 2

int Steps = 0;

/* Pasos en modo wave drive, para poder mantener el delay entre pasos al mínimo */
byte Paso[4][4] = { 
  {1, 0, 0, 0},
  {0, 1, 0, 0},
  {0, 0, 1, 0},
  {0, 0, 0, 1}
};

void setup() {
  Serial.begin(115200);
  
  pinMode(BUZZER, OUTPUT);
  
  pinMode(IRL, INPUT);
  pinMode(IRM, INPUT);
  pinMode(IRR, INPUT);
  
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);

  /* Mover el motor hasta encontrar una nota */ 
  while (not existeNota()) {
    darUnPaso(4);
  }
  /* Delay adicional para leer el centro de la nota */
  for (int i = 0; i < 100; i++) {
    darUnPaso();
  }
}

void loop() {
  leerNota();
  //delay(1000);
  turnOnce();
  //delay(1000);
}

void turnOnce() {
  for (int i = 0; i < 400; i++) {
    darUnPaso();
  }
}

boolean existeNota() {
  for (int i = 0; i < READERS_LEN; i++) {
    if (digitalRead(READERS[i]) == HIGH) {
      return true;
    }
  }
  return false;
}

void leerNota() {
  int duration = 500; // milisegundos
  boolean sharp = false;
  int octave = 5;
  /* código de prueba para codificar notas más complejas
  if (digitalRead(IRL) == HIGH) {
    duration = duration * 2;
  }
  if (digitalRead(IRM) == HIGH) {
    sharp = true;
  }
  if (digitalRead(IRR) == HIGH) {
    octave = octave + 1;
  }

  turnOnce();
  */
  float note = 0;
  // Cuando la salida del lector es HIGH, es porque no detecta objeto, o en este caso no hay reflexión por ser una superficie oscura
  for (int i = 0; i < READERS_LEN; i++) {
    if (digitalRead(READERS[i]) == HIGH) {
      note += pow(2, i);
    }
  }
  float freq = 0;
  if (sharp) {
    freq = SHARP[int(ceil(note))];
  } else {
    freq = NOTES[int(ceil(note))];
  }
  Serial.println(ceil(int(note)));
  Serial.println(pow(2, octave) * freq);
  tone(BUZZER, pow(2, octave) * freq, duration);
}

void darUnPaso() {
  digitalWrite(IN1, Paso[Steps][0]);
  digitalWrite(IN2, Paso[Steps][1]);
  digitalWrite(IN3, Paso[Steps][2]);
  digitalWrite(IN4, Paso[Steps][3]);
  Steps += 1;
  Steps = Steps % 4;
  delay(STEP_DELAY);
}

void darUnPaso(int stepDelay) {
  digitalWrite(IN1, Paso[Steps][0]);
  digitalWrite(IN2, Paso[Steps][1]);
  digitalWrite(IN3, Paso[Steps][2]);
  digitalWrite(IN4, Paso[Steps][3]);
  Steps += 1;
  Steps = Steps % 4;
  delay(stepDelay);
}
